##Grocer App
####This is a continuation of the app created in assessment 2 in order to complete the basic single page application

###21 Sept commit

  * Got the parameterized routing edit button from overview to work
  * Populated edit product fields onto form

####To do
  
  * To get route working for update database button and investigate console error messages
  * To get barcode image displaying on the overview tab
  * To get the search bar working for names
  * To get filter by brands working

###Last Commit @ 5.30pm

  * Sql statement for edit product works on workbench

####Unresolved issues
  * Sort and Filter functions do not work
  * Could not traverse to edit page (Page2) with parametrized routing
  * No UPC12 barcode image displayed (Low priority)

###Third Commit @ 3pm

  * Successfully completed and tested addProduct function
  * Sort and filter bars do not work

###Second Commit @ 1pm

  * Successfully populated data fields onto overview
  * Some issues with refactoring requires attention

###First Commit @ 11am

  * Scafolding complete
  * To test $state

