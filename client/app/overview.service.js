(function() {
  angular
    .module("GrocerApp")
    .service("OverviewSvc", OverviewSvc);

  OverviewSvc.$inject = ["$http", "$q"];

  function OverviewSvc($http, $q) {
    var overviewSvc = this; // vm

    // Sorting shenanigans
    // overviewSvc.reverse = false;
    // overviewSvc.propertyName = "-id"; // this gets overwritten

    // Instantiating functions
    overviewSvc.display20Products = display20Products;
    // overviewSvc.editProduct = editProduct;

    // SQL statements here

    function display20Products() {
      var defer = $q.defer();

      $http.get("/overview")
        .then(function (result) {
          defer.resolve(result.data);
        })
        .catch (function (error) {
          defer.reject(error);
        })
        return (defer.promise);    
    }

    // not used
    // function getProduct(index) {
    //   overviewSvc.id = result[index].id;
    //   overviewSvc.upc12 = result[index].upc12;
    //   overviewSvc.brand = result[index].brand;
    //   overviewSvc.name = result[index].name;
    // }



    // function sortBy(propertyName){
    //   overviewSvc.reverse = (overviewSvc.propertyName === propertyName) ? !overviewSvc.reverse: false;
    //   overviewSvc.propertyName = propertyName;
    // }

    // function filterBy(propertyName) {
    //   // code here
    // }

  }

}) ();