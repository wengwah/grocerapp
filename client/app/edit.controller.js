(function () {
  "use strict";
  angular
    .module("GrocerApp")
    .controller("EditCtrl", EditCtrl);

  EditCtrl.$inject = ["$state", "EditSvc"];

  function EditCtrl($state, EditSvc) {
    var editCtrl = this; // vm

    // initializing variables
    editCtrl.product = {};
    editCtrl.id = $state.params.id;

    // EditCtrl.updateProduct = EditSvc.updateProduct

    EditSvc.getProductById(editCtrl.id)
      .then(function (result) {
        editCtrl.product = result;
        console.log(result);
      })
      .catch(function (error) {
        console.log(error);
      });

    EditSvc.updateProduct(editCtrl.product)
      .then(function (result) {
        console.log(results);
        console.log("Update success!");
      })
      .catch(function (err) {
        console.log(editCtrl.product);
        console.log(err)
      });
      


  }

  // GrocerApp.config(GrocerConfig);
  // GrocerApp.service("EditSvc", EditSvc);


}) ();