(function() {
  angular
    .module("GrocerApp")
    .service("EditSvc", EditSvc);

  EditSvc.$inject = ["$http", "$q"];

  function EditSvc($http, $q) {
    var editSvc = this; // overviewSvc

    // Instantiating functions
    editSvc.getProductById = getProductById;
    editSvc.updateProduct = updateProduct;

    // SQL statements here

    // this retrieves the product from the database by id
    function getProductById(id) {
      var defer = $q.defer();

      $http.get("/edit/" + id)
        .then(function (result) {

          defer.resolve(result.data);
        })
        .catch (function (error) {
          defer.reject(error);
        })
      return (defer.promise);
    
    }

    function updateProduct(product) {
      var defer = $q.defer();
      const id = product.id;
      
      $http.put("/edit/" + id, { product: product })
        .then(function (result) {
          // console.log(result);
          defer.resolve(result);
        })
        .catch(function (err) {
          defer.reject(err);
        });

      return defer.promise;
    }

  }

}) ();